// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyBuaKMgen5vx2uyMnfw5jtG2n2sVPnnimg",
    authDomain: "chicaies.firebaseapp.com",
    databaseURL: "https://chicaies.firebaseio.com",
    projectId: "chicaies",
    storageBucket: "chicaies.appspot.com",
    messagingSenderId: "318832987892",
    appId: "1:318832987892:web:fbeee23757fe0806"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
