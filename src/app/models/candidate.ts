export interface Candidate {
  id: string;
  name: string;
  grupo: string;
  carrera: string;
  gender: string;
  pasarela: number;
  pregunta: number;
  expresionCorporalOral: number;
  dominioEscenico: number;
  photo: string;
}
