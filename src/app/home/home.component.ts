import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('user_id') === null) {
      this.router.navigate(['login']).then(() => {});
    }
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['login']).then(() => {});
  }
}
