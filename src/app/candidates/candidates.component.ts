import { Component, OnInit } from '@angular/core';
import {AngularFirestore, DocumentChangeAction} from '@angular/fire/firestore';
import {first} from 'rxjs/operators';
import {CandidatesService} from '../services/candidates.service';
import {Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {

  extendedTemp = [];
  extendedItems = [];

  constructor(private db: AngularFirestore,
              private router: Router,
              private location: Location,
              private candidatesService: CandidatesService) {}

  ngOnInit() {
    this.db
      .collection('candidatos')
      .snapshotChanges()
      .pipe(first())
      .subscribe((res: DocumentChangeAction<any>[]) => {
        res.forEach((result: DocumentChangeAction<any>) => {
          const data = result.payload.doc.data();
          this.extendedItems.push({
            id: result.payload.doc.id,
            data
          });

          this.extendedTemp.push({
            id: result.payload.doc.id,
            data
          });
        });
      });
  }

  changeSelection(event) {
    if (event.value == 0) this.extendedItems = this.extendedTemp;
    if (event.value == 1) this.extendedItems = this.extendedTemp.filter(element => element.data.gender == 'M');
    if (event.value == 2) this.extendedItems = this.extendedTemp.filter(element => element.data.gender == 'F');
  }

  goProfile(candidate) {
    this.candidatesService.currentCandidates = candidate;
    this.router.navigate(['profile']).then(() => {});
  }

  goVote(candidate) {
    this.candidatesService.currentCandidates = candidate;
    this.router.navigate(['vote']).then(() => {});
  }

  search(event) {
    let value = event.target.value;

    this.extendedItems = this.extendedTemp.filter((element) => {
      return element.data.name.toLowerCase().indexOf(value) !== -1 || !value;
    });
  }

  goBack() {
    this.location.back();
  }
}
