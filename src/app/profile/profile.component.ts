import { Component, OnInit } from '@angular/core';
import {CandidatesService} from '../services/candidates.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  currentCandidate: any = {};

  constructor(private location: Location,
              private candidatesService: CandidatesService) { }

  ngOnInit() {
    if (this.candidatesService.currentCandidates === undefined) this.location.back();
    this.currentCandidate = this.candidatesService.currentCandidates;
  }

  goBack() {
    this.location.back();
  }
}
