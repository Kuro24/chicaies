import { Component, OnInit } from '@angular/core';
import {CandidatesService} from '../services/candidates.service';
import {AngularFirestore, DocumentChangeAction} from '@angular/fire/firestore';
import {first} from 'rxjs/operators';
import {Location} from '@angular/common';

@Component({
  selector: 'app-vote-candidate',
  templateUrl: './vote-candidate.component.html',
  styleUrls: ['./vote-candidate.component.css']
})
export class VoteCandidateComponent implements OnInit {

  currentCandidate: any = {};
  value: string = '';
  punctuation: number = 0;

  error: string = undefined;
  votes = [];

  constructor(private db: AngularFirestore,
              private location: Location,
              private candidatesService: CandidatesService) {
    this.db
      .collection('votes')
      .snapshotChanges()
      .pipe(first())
      .subscribe((res: DocumentChangeAction<any>[]) => {
        res.forEach((result: DocumentChangeAction<any>) => {
          const data = result.payload.doc.data();
          this.votes.push({
            id: result.payload.doc.id,
            data
          });
        });
      });
  }

  ngOnInit() {
    this.currentCandidate = this.candidatesService.currentCandidates;
  }

  selectCategory(event) {
    this.value = event.value;
  }

  sendVote() {
    this.error = undefined;

    if (this.punctuation > 25) {
      this.error = "La puntuacion Maxima es de 25";
      return;
    }

    if (this.punctuation <= 0) {
      this.error = "La puntuacion no puede ser menor que 1";
      return;
    }

    switch (this.value) {
      case '1':
        this.addVote('Dominio y Desenvolvimiento Escénico');
        break;
      case '2':
        this.addVote('Expresión Corporal y Oral');
        break;
      case '3':
        this.addVote('Pasarela');
        break;
      case '4':
        this.addVote('Pregunta');
        break;
    }
  }

  addVote(category: string) {
    let data = {
      usuario: localStorage.getItem('user_id'),
      category: category,
      carnet: this.currentCandidate.id,
      vote: this.punctuation
    };

    let items = this.db.collection('votes');
    let item_list = items.snapshotChanges();

    if (
      this.votes.some(element => {
        return element.data.category == data.category &&
          element.data.usuario == data.usuario &&
          element.data.carnet == data.carnet
      })
    ) {
      this.error = 'Ya ha votado por ese participante en esta categoria:' + category;
      return;
    } else {
      items.add(data).then(() => {}, () => {});
      this.location.back();
    }
  }
}
