import {Component, HostListener, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {AngularFirestore, DocumentChangeAction} from '@angular/fire/firestore';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

  width: number = 0;

  candidateList = [];
  candidateTempList = [];
  votesList = [];

  constructor(private location: Location,
              private db: AngularFirestore,) {
  }

  ngOnInit() {
    this.width = window.innerWidth;

    this.asyncVotes().then(() => {
      this.asyncCollection().then(() => {
        console.log('ordena por favr');
        this.candidateList.sort((a,b) => (a.total < b.total) ? 1 : ((b.total < a.total) ? -1 : 0));
      });
    });
  }

  async asyncVotes() {
    this.db.collection('votes').snapshotChanges().pipe(first()).subscribe((res: DocumentChangeAction<any>[]) => {
      res.forEach((result: DocumentChangeAction<any>) => {
        const data = result.payload.doc.data();
        this.votesList.push({ id: result.payload.doc.id, data });
      });
    });
  }

  async asyncCollection() {
    this.db.collection('candidatos').snapshotChanges().pipe(first()).subscribe((res: DocumentChangeAction<any>[]) => {
      res.forEach((result: DocumentChangeAction<any>) => {
        const data = result.payload.doc.data();
        this.candidateList.push({ id: result.payload.doc.id, data, total: this.getPoints({id: result.payload.doc.id, data}) });
        this.candidateTempList.push({ id: result.payload.doc.id, data, total: this.getPoints({id: result.payload.doc.id, data}) });

        this.candidateList.sort((a,b) => (a.total < b.total) ? 1 : ((b.total < a.total) ? -1 : 0));
        this.candidateTempList.sort((a,b) => (a.total < b.total) ? 1 : ((b.total < a.total) ? -1 : 0));
      });
    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
  }

  filterList(event) {
    let value = event.value;

    if (value == '0')
      this.candidateList = this.candidateTempList;
    if (value == '1') {
      this.candidateList = this.candidateTempList.filter(element => element.data.gender == "M");
      this.candidateList.sort((a,b) => (a.total < b.total) ? 1 : ((b.total < a.total) ? -1 : 0));
    }
    if (value == '2') {
      this.candidateList = this.candidateTempList.filter(element => element.data.gender == "F");
      this.candidateList.sort((a,b) => (a.total < b.total) ? 1 : ((b.total < a.total) ? -1 : 0));
    }
  }

  getDominio(candidate) {
    let temp_points = this.votesList.filter(element =>
      element.data.carnet == candidate.id && element.data.category == 'Dominio y Desenvolvimiento Escénico');

    if (temp_points.length > 0) {
      return temp_points.reduce((a, b) => {
        return (a.hasOwnProperty('data') ? a.data.vote : a) + b.data.vote;
      }, 0);
    } else return 0;
  }

  getExpresion(candidate) {
    let temp_points = this.votesList.filter(element =>
      element.data.carnet == candidate.id && element.data.category == 'Expresión Corporal y Oral');

    if (temp_points.length > 0) {
      return temp_points.reduce((a, b) => {
        return (a.hasOwnProperty('data') ? a.data.vote : a) + b.data.vote;
      }, 0);
    } else return 0;
  }

  getPasarela(candidate) {
    let temp_points = this.votesList.filter(element =>
      element.data.carnet == candidate.id && element.data.category == 'Pasarela');

    if (temp_points.length > 0) {
      return temp_points.reduce((a, b) => {
        return (a.hasOwnProperty('data') ? a.data.vote : a) + b.data.vote;
      }, 0);
    } else return 0;
  }

  getPregunta(candidate) {
    let temp_points = this.votesList.filter(element =>
      element.data.carnet == candidate.id && element.data.category == 'Pregunta');

    if (temp_points.length > 0) {
      return temp_points.reduce((a, b) => {
        return (a.hasOwnProperty('data') ? a.data.vote : a) + b.data.vote;
      }, 0);
    } else return 0;
  }

  getPoints(candidate) {
    let temp_points = this.votesList.filter(element => element.data.carnet == candidate.id);

    if (temp_points.length > 0) {
      return temp_points.reduce((a, b) => {
        return (a.hasOwnProperty('data') ? a.data.vote : a) + b.data.vote;
      }, 0);
    } else return 0;
  }

  goBack() {
    this.location.back();
  }
}
