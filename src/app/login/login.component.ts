import { Component, OnInit } from '@angular/core';
import {AngularFirestore, DocumentChangeAction} from '@angular/fire/firestore';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide: boolean = true;
  userList = [];

  username: string = '';
  password: string = '';

  error = undefined;

  constructor(private router: Router,
              private db: AngularFirestore,) {

    if (localStorage.getItem('user_id') !== null) {
      console.log('go home');
      this.router.navigate(['home']).then(() => {});
      return;
    }
  }

  ngOnInit() {
    this.db
      .collection('usuarios')
      .snapshotChanges()
      .pipe(first())
      .subscribe((res: DocumentChangeAction<any>[]) => {
        res.forEach((result: DocumentChangeAction<any>) => {
          const data = result.payload.doc.data();
          this.userList.push({
            id: result.payload.doc.id,
            data
          });

          this.userList.push({
            id: result.payload.doc.id,
            data
          });
        });
      });
  }

  login() {
    this.error = undefined;

    if (this.username == '') {
      this.error = 'El nombre de usuario no puede estar vacio';
      return;
    }

    if (this.password == '') {
      this.error = 'La contraseña no puede estar vacia';
      return;
    }

    let user = this.userList.find(element =>
      (element.data.username == this.username.toLowerCase() && element.data.password == this.password.toLowerCase()));

    if (user != undefined) {
      localStorage.setItem('user_id', user.id);

      this.router.navigate(['/home']).then(() => {});
    } else {
      this.error = 'El usuario y la contraseña son incorrectas'
    }
  }
}
